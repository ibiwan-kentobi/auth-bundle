<?php
/**
 * Created by PhpStorm.
 * User: jtangas
 * Date: 11/21/17
 * Time: 10:46 AM
 */

namespace Jtangas\AuthBundle\Composer;


use ArrayIterator;
use Symfony\Component\Process\InputStream;
use Symfony\Component\Process\Process;
use Symfony\Component\VarDumper\VarDumper;
use Symfony\Component\Yaml\Yaml;

class ScriptHandler
{
    const DEFAULT_PRIVATE   = '/private.pem';
    const DEFAULT_PUBLIC    = '/public.pem';
    const LOCAL_DIR         = '/var/jwt';

    const MAKE_DIR          = 'mkdir -p %s';
    const GEN_RSA           = 'openssl genrsa -passout file:%s -out %s -aes256 4096';
    const PUB_OUT           = 'openssl rsa -passin file:%s -pubout -in %s -out %s';
    const PARAMS_FILE       = 'auth-bundle.parameters.yml';

    public static function injectConfig()
    {
        self::updateMainConfig();
        self::updateSecurity();
        self::updateRouting();
    }

    public static function createJWTCert()
    {
        $rootPath   = realpath(dirname(__FILE__)) . '/../../';
        $passwordFile = $rootPath . 'passfile';
        $appPathParts = explode("vendor", realpath(dirname(__FILE__)));
        $appPath = rtrim(current($appPathParts), '/');

        try {
            $config     = Yaml::parse(file_get_contents($rootPath . 'src/Resources/config/' . self::PARAMS_FILE));

            $jwtPrivateTokenPath = str_replace("%kernel.project_dir%", $appPath, $config['parameters']['jwt_private_key_path']);
            $jwtPublicTokenPath = str_replace("%kernel.project_dir%", $appPath, $config['parameters']['jwt_public_key_path']);

            $privatePem = $jwtPrivateTokenPath;
            $publicPem  = $jwtPublicTokenPath;

            if (file_exists($privatePem) && file_exists($publicPem)) {
                echo "JWT keys already configured" . PHP_EOL;
            } else {
                $cmd = sprintf(self::MAKE_DIR, $appPath . self::LOCAL_DIR);

                $process = new Process($cmd);
                $process->run();

                if (isset($config['parameters']['jwt_key_pass_phrase']) && $config['parameters']['jwt_key_pass_phrase']) {

                    $pass   = (string)$config['parameters']['jwt_key_pass_phrase'];
                    $passFile = fopen($passwordFile, 'w+');
                    fwrite($passFile, $pass);
                    fclose($passFile);

                    $cmd    = sprintf(self::GEN_RSA, $passwordFile, $privatePem);
                    $process = new Process($cmd);
                    $process->disableOutput();
                    echo "Generating JWT key files" . PHP_EOL;
                    $process->run();
                } else {
                    throw new \Exception('a password is needed to generate an rsa key');
                }

                $pubOutCmd = sprintf(self::PUB_OUT, $passwordFile, $privatePem, $publicPem);
                $process = new Process($pubOutCmd);
                $process->disableOutput();
                $process->run();

                if ($process->isSuccessful()) {
                    echo "JWT key files generated successfully" . PHP_EOL;
                }

                unlink($passwordFile);
            }
        } catch (\Exception $e) {
            VarDumper::dump($e->getMessage());
            if (file_exists($passwordFile)) {
                unlink($passwordFile);
            }
        }
    }

    private static function updateMainConfig()
    {
        $rootPath   = realpath(dirname(__FILE__)) . '/../../';
        $appPathParts = explode("vendor", realpath(dirname(__FILE__)));
        $appPath = rtrim(current($appPathParts), '/');

        $rootConfigFile = $appPath . '/app/config/config.yml';

        $rootConfig = Yaml::parse(file_get_contents($rootConfigFile));
        $imports = $rootConfig['imports'];
        $files = array_column($imports, "resource");
        if (!in_array($rootPath . 'src/Resources/config/config.yml', $files)) {
            $rootConfig['imports'][] = ['resource' => $rootPath . 'src/Resources/config/config.yml'];
        }

        $rootConfigureYaml = Yaml::dump($rootConfig, 5);

        $fh = fopen($rootConfigFile, "w+");
        fwrite($fh, $rootConfigureYaml);
        fclose($fh);
    }

    private static function updateSecurity()
    {
        $rootPath   = realpath(dirname(__FILE__)) . '/../../';
        $appPathParts = explode("vendor", realpath(dirname(__FILE__)));
        $appPath = rtrim(current($appPathParts), '/');

        $securityConfigFile = $appPath . '/app/config/security.yml';
        $securityConfig = Yaml::parse(file_get_contents($securityConfigFile));

        $securityOverrides = $rootPath . 'src/Resources/config/security.yml';
        $sOverrideConfig = Yaml::parse(file_get_contents($securityOverrides));

        $newData = [];
        $newData['security'] = array_merge($securityConfig['security'], $sOverrideConfig['security']);

        $firewallDefaults = $rootPath . 'src/Resources/config/jwt_firewall_default.yml';
        $firewallConfig = Yaml::parse(file_get_contents($firewallDefaults));

        if (!isset($newData['security']['firewalls'])) {
            $newData['security']['firewalls'] = [];
        }

        foreach ($firewallConfig['firewalls'] as $firewall => $data) {
            if (isset($newData['security']['firewalls']) && array_key_exists($firewall, $newData['security']['firewalls'])) {
                continue;
            }

            $firewalls = array_merge($newData['security']['firewalls'], [$firewall => $data]);

            ksort($firewalls);

            $newData['security']['firewalls'] = $firewalls;
        }

        $securityConfigYaml = Yaml::dump($newData,5);
        $fh = fopen($securityConfigFile, "w+");
        fwrite($fh, $securityConfigYaml);
        fclose($fh);
    }

    private static function updateRouting()
    {
        $appPathParts = explode("vendor", realpath(dirname(__FILE__)));
        $appPath = rtrim(current($appPathParts), '/');

        $routingConfigFile = $appPath . '/app/config/routing.yml';
        $routingConfig = Yaml::parse(file_get_contents($routingConfigFile));

        if (!array_key_exists('fos_user', $routingConfig)) {
            $routingConfig['fos_user'] = ['resource' =>  "@FOSUserBundle/Resources/config/routing/all.xml"];
        }

        $routeConfig = Yaml::dump($routingConfig, 5);
        $fh = fopen($routingConfigFile, 'w+');
        fwrite($fh, $routeConfig);
        fclose($fh);
    }
}