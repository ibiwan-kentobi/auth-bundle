<?php
/**
 * Created by PhpStorm.
 * User: jtangas
 * Date: 12/20/17
 * Time: 10:11 AM
 */

namespace Jtangas\AuthBundle\EventListener;


use FOS\UserBundle\Model\UserInterface;
use Jtangas\UtilityBundle\Factory\DataModelFactory;
use Jtangas\UtilityBundle\Utility;
use Lexik\Bundle\JWTAuthenticationBundle\Event\JWTCreatedEvent;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\VarDumper\VarDumper;

class JwtCreatedListener
{
    protected $formatter;
    protected $requestStack;
    protected $ttlAdmin;
    protected $ttlUser;
    protected $userResponse;

    public function __construct(
        RequestStack $requestStack,
        $ttlAdmin,
        $ttlUser,
        DataModelFactory $formatter,
        $userResponseClass = 'Jtangas\UtilityBundle\DataModel\UserResponse'
    ) {
        $this->requestStack = $requestStack;
        $this->ttlAdmin     = $ttlAdmin;
        $this->ttlUser      = $ttlUser;
        $this->formatter    = $formatter;
        $this->userResponse = $userResponseClass;
    }

    public function onJwtCreated(JWTCreatedEvent $event)
    {
        $request = $this->requestStack->getCurrentRequest();

        $payload = $event->getData();
        /** @var UserInterface $user */
        $user = $event->getUser();

        $ttl = $this->ttlUser;
        if ($user->isSuperAdmin()) {
            $ttl = $this->ttlAdmin;
        }

        if ($request->query->has('ttl')) {
            $ttl = $request->query->get('ttl');
            if (!is_numeric($ttl)) {
                throw new \Exception("invalid ttl provided", 409);
            }
        }

        $payload['ttl'] = $ttl;
        $payload['exp'] = time() + $ttl;
        $payload['ip'] = Utility::getClientIp($request);
        $payload['data'] = $this->formatter->format($user, $this->userResponse);

        $event->setData($payload);
    }
}